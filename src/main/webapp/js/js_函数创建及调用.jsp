<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2019/7/31
  Time: 15:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>js_函数创建及调用</title>
</head>
<body>

</body>
<script>
    //使用function 函数名称(参数列表 ){函数体;}方式来创建函数
    function add(a, b) {
        // alert(a + b);
        //在函数中如果要获取所有参数列表中的信息可以使用argument来操作
        /*for(var i=0;i<arguments.length;i++){
            alert(arguments[i]);
        }*/
        //返回a与b的和
        return a+b;

    }
    //调用函数
    var sum = add(30,10, 20,50);
    alert(sum);

    //使用 var 函数名称=function(参数列表){函数体;}来创建函数
    /*var add = function(a, b) {
        alert(a + b);
    }*/
    //函数调用
    // add(10, 20);

    //  var 函数名称=new Function(参数列表,函数体);
    /*var add = new Function("a", "b", "alert(a+b)");
    add(10, 20);*/



</script>
</html>
