<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2019/8/1
  Time: 9:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>js_onload</title>
    <style type="text/css">
        #d1{width:200px;height: 200px;background-color: red;}
    </style>
    <script type="text/javascript">
        /*window.onload = function(){
            var span = document.getElementById("span");
            alert(span);
            span.innerHTML = "<p>hello js</p>"; //  <h1>
        };*/
        var fun = function(){
            var span = document.getElementById("span");
            alert(span);
            span.innerHTML = "<p>hello js</p>"; //  <h1>
        };
        fun();
    </script>

</head>
<body>
<div id="d1" ><span id="span"></span></div>
</body>
</html>
