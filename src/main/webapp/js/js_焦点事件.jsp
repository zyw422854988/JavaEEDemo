<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2019/7/31
  Time: 17:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>js_焦点事件</title>
    <script>
        function focusEvent() {
            // debugger
            //友好提示
            var span = document.getElementById('action');
            span.innerHTML = "用户名格式最小8位";
            span.style.color = "green";
        }
        function blurEvent(){
            // debugger
            //错误提示
            var span = document.getElementById('action');
            span.innerHTML = "对不起 格式不正确";
            span.style.color = "red";
        };

    </script>
</head>
<body>
请输入：
<input id="inputTxt" type="text" onfocus="focusEvent()" onblur="blurEvent()"/>
<span id="action"></span>

</body>

</html>
