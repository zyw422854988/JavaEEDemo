<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2019/7/31
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>js_二级联动</title>
</head>
<body>
<select id="city" onchange="changeCity()">
    <option id="bj" value="bj">北京</option>
    <option id="tj" value="tj">天津</option>
    <option id="sh" value="sh">上海</option>
</select>
<select id="area">
    <option>海淀区</option>
    <option>朝阳区</option>
    <option>昌平区</option>
</select>
</body>
<script>
    function changeCity() {
        var select = document.getElementById("city");
        var optionVal = select.value;
        console.log(optionVal)
        switch(optionVal){
            case 'bj':
                var area = document.getElementById("area");
                area.innerHTML = "<option>海淀</option><option>朝阳</option><option>东城</option>";
                break
            case 'tj':
                var area = document.getElementById("area");
                area.innerHTML = "<option>南开</option><option>西青</option><option>河西</option>";
                break;
            case 'sh':
                var area = document.getElementById("area");
                area.innerHTML = "<option>浦东</option><option>杨浦</option>";
                break;
            default:
                break
        }
    }
</script>
</html>
