<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2019/8/1
  Time: 9:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>js_window</title>
</head>
<body>
<span id="second" style="color: red;">5</span>秒后跳转到首页，如果不跳转请<a href="/index.jsp">点击这里</a>
</body>
<script>
    /*alert(window.innerHeight)
    alert(window.innerWidth)

    alert(window.location)

    window.close()
    alert(window.closed)*/

    /*var prompt = window.prompt("请输入密码？")
    alert(prompt);*/

    /*window.alert("window.alert")
    var result = window.confirm("您确认要删除吗？")
    alert(result)*/

    // window.open("http://www.baidu.com")

    /*var timer = setTimeout(
        function () {
            alert("ddd")
        },
        50
    )

    clearTimeout(timer)*/

    /*var timer2 = setInterval(
        function(){
            alert("nihao");
        },
        3000
    );

    clearInterval(timer2);*/


    var time = 5;
    var timer;
    timer = setInterval(
        function(){
            var second = document.getElementById("second");
            if(time>=1){
                second.innerHTML = time;
                time--;
            }else{
                clearInterval(timer);
                location.href="/index.jsp";
            }
        },
        1000
    );
</script>
</html>
