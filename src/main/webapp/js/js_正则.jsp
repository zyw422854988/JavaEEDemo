<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2019/7/31
  Time: 14:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>js_正则</title>
</head>
<body>

</body>
<script>
    // 单个字符
    var str = "dcba";
    var reg = /^[bd]/;
    // alert(reg.test(str))

    // 单个数字字符
    var str2 = "fdsasdg533246436fdfgdsg";
    var reg2 = /^\d/;
    var reg3 = /^\w/;
    // alert(reg3.test(str2))

    // 量词
    var str4 = "bacad";
    var reg4 = /a+/
    // alert(reg4.test(str4))

    // {m, n }
    var str5 = "ddabcdde"
    var reg5 = /^d{1,}/
    // alert(reg5.test(str5))

    var reg6 = new RegExp("^d{1,}");

    // alert(reg6.test(str5))


    // 校验邮箱
    var email = "haohao_827@163.com";
    var reg = /^[A-z]+[A-z0-9_-]*@[A-z0-9]+.[A-z]+$/;
    // alert(reg.test(email));

    // 转义

    var str7 = "fsad f.sfs    af";
    alert(str7.replace(/\s*/g,""))
    var reg7 = new RegExp(".");
    alert(reg7.test(str7))



</script>
</html>
