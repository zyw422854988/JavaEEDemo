public class B {
    private Integer age;
    private A a = new A();

    public Integer getAge () {
        return age;
    }

    public void setAge (Integer age) {
        this.age = age;
    }

    public A getA () {
        return a;
    }

    public void setA (A a) {
        this.a = a;
    }
}
