import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MathDemo {
    public static void main (String[] args) {

        // 向上取整
        // System.out.println(Math.ceil(8.5));
        // 向下取整
        // System.out.println(Math.floor(8.5));
        // 四舍五入
        // System.out.println(Math.round(8.5));


        List<Integer> arrayList = Arrays.asList(1,2,3,4,5);


        System.out.println(arrayList.toString());

    }
}
