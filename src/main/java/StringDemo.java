public class StringDemo {
    public static void main (String[] args) {
        // 不可变 --

        String aa = "abc";
        String bb = new String("abc");

        // System.out.println(aa == bb);

        Integer num01 = 127;
        Integer num02 = 127;
        System.out.println(num01 == num02);

        Integer num03 = 128;
        Integer num04 = 128;
        System.out.println(num03 == num04);

        Integer num05 = -128;
        Integer num06 = -128;
        System.out.println(num05 == num06);

        Integer num07 = -129;
        Integer num08 = -129;
        System.out.println(num07 == num08);
    }
}
