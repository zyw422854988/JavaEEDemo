public class SwitchDemo {

    public static void main (String[] args) {
        // long aa = 10;  // 不行
        // byte aa = 10;  // 可以
        // short aa = 10;    // 可以
        // char aa = '2';    // 可以
        // boolean aa = true;  // 不可以
        // float aa = 2;
        String aa = "2";
        switch (aa){

            case "2":
                break;

                default:
                    System.out.println(aa);
                    break;
        }
    }
}
