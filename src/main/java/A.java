public class A {
    private String name;
    private B b =new B();

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public B getB () {
        return b;
    }

    public void setB (B b) {
        this.b = b;
    }
}
